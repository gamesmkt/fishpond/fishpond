<?php

namespace Gamesmkt\Fishpond\Test;


interface GetRecordDetailUrl
{
    /** @test */
    public function getRecordDetailUrl();
}
