<?php

namespace Gamesmkt\Fishpond\Test;


interface CanFetchRecordsByContextTest
{
    /** @test */
    public function fetchRecordsByContext();
}
