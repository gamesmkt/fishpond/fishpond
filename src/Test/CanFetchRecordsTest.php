<?php

namespace Gamesmkt\Fishpond\Test;


interface CanFetchRecordsTest
{
    /** @test */
    public function fetchRecords();
}
