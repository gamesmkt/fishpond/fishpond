<?php

namespace Gamesmkt\Fishpond\Test;


interface CanFetchRecordsByDirectWithMarkTest
{
    /** @test */
    public function fetchRecordsByDirectWithMark();
}
