<?php

namespace Gamesmkt\Fishpond;

use InvalidArgumentException;
use ReflectionClass;

class RecordType implements TypeInterface
{
    /** @var int */
    private $type;

    /**
     * @param int $type The record type
     */
    public function __construct(
        int $type
    ) {
        $this->assertType($type);

        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    private function assertType($type)
    {
        if (!is_int($type) || $type < 0) {
            throw new InvalidArgumentException('Type must be a positive integer.');
        }

        switch ($type) {
            case self::RECORD_BET:
                break;
            case self::RECORD_DONATE:
                break;
            case self::RECORD_TRANSFER:
                break;
            default:
                throw new InvalidArgumentException("Not support [$type] type.");
        }
    }

    public function __toString()
    {
        return $this->type;
    }

    public function getErrorMessage()
    {
        $class = new ReflectionClass(__CLASS__);
        $constants = array_flip($class->getConstants());

        return $constants[$this->getType()];
    }
}
