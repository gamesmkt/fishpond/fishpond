<?php

namespace Gamesmkt\Fishpond\Adapter\Polyfill;


use Gamesmkt\Fishpond\Exception\NotSupportingException;
use Gamesmkt\Fishpond\TypeInterface;

trait CheckSupportedGameTypesTrait
{
    protected function checkSupportedGameTypes(array $validType, TypeInterface $type = null)
    {
        if (isset($type) && !in_array($type->getType(), $validType)) {
            throw new NotSupportingException(get_class() . ' does not support the [' . $type->getErrorMessage() . '] type.');
        }
    }
}
