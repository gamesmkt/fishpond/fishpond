<?php

namespace Gamesmkt\Fishpond\Adapter\Polyfill;


use Gamesmkt\Fishpond\Config;
use Gamesmkt\Fishpond\Exception\NotSupportingException;
use Gamesmkt\Fishpond\GameInterface;
use Gamesmkt\Fishpond\PlayerInterface;

trait NotSupportingLogoutTrait
{
    /**
     * 登出玩家。
     *
     * @param \Gamesmkt\Fishpond\PlayerInterface $player
     * @param \Gamesmkt\Fishpond\GameInterface $game
     * @param \Gamesmkt\Fishpond\Config $config
     *
     * @return bool
     */
    public function logout(PlayerInterface $player, GameInterface $game, Config $config)
    {
        throw new NotSupportingException(
            get_class($this) . ' does not support logout.'
        );
    }
}