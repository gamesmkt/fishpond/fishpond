<?php

namespace Gamesmkt\Fishpond;


interface TestInterface
{
    /**
     * @beforeClass
     */
    public static function setupBefore();

    /** @test */
    public function createMember();

    /** @test */
    public function login();

    /** @test */
    public function logout();

    /** @test */
    public function balance();

    /** @test */
    public function transfer();

    /** @test */
    public function checkTransfer();

    /** @test */
    public function gameList();
}
